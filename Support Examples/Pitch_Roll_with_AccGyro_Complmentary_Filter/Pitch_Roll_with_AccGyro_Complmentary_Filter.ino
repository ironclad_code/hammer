#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BNO055.h>
#include <utility/imumaths.h>
#include <math.h>

#define BNO055_SAMPLERATE_DELAY_MS (10)

Adafruit_BNO055 myIMU = Adafruit_BNO055();

//Accelorometer Variables
double Pitch_R = 0; //Pitch from Accelorometer in Radians
double Roll_R = 0; //Roll from Acc in Radians
double Pitch_A = 0; //Pitch from Accelorometer in degrees
double Roll_A = 0; //Roll from Accelorometer in degrees
double Roll_F = 0; //Roll from Accelorometer Filtered in Degrees
double Pitch_F = 0; //Pitch from Accelorometer Filtered in Degrees
double Roll_F_Pre = 0; //Previous Roll from Accelorometer Filtered in Degrees
double Pitch_F_Pre = 0; //Previous Pitch from Accelorometer Filtered in Degrees
double PercentFilter = 90; //Percent to prioritize the previous values over the current value
bool Pitch_A_GOOD = false;

//Gyro Variables
double PitchG_R;
double RollG_A = 0;
double PitchG_A = 0;
unsigned long TimeStamp_Pre = millis();
unsigned long TimeStamp;
double dt;
bool PitchG_A_GOOD = false;
double Pitch_Angle_Change;

//LED Variables
unsigned long Toggle_Time = millis();
bool Red_State = HIGH;
bool White_State = HIGH;
bool Blue_State = HIGH;
unsigned long CountDown;
int Red_Pin = 12;
int White_Pin = 11;
int Blue_Pin = 10;

//Filter Varables
float Percent_Use_G = 95;
double PitchF_A = 0;

void setup() {
  // put your setup code here, to run once:
//start Serial
Serial.begin(115200);

//Start IMU
myIMU.begin();
delay(1000);
myIMU.setExtCrystalUse(true);
pinMode(12, OUTPUT);
pinMode(11, OUTPUT);
pinMode(10, OUTPUT);
}

void loop() {

//Read Calibration Levels
  //uint8_t sys_C, gyro_C, accel_C, mg_C =0;
  //myIMU.getCalibration(&sys_C, &gyro_C, &accel_C, &mg_C);


// Acquire Vectors from IMU
  imu::Vector<3> acc = myIMU.getVector(Adafruit_BNO055::VECTOR_ACCELEROMETER);
  imu::Vector<3> gyro = myIMU.getVector(Adafruit_BNO055::VECTOR_GYROSCOPE);
  //imu::Vector<3> mag = myIMU.getVector(Adafruit_BNO055::VECTOR_ACCELEROMETER);
  
 // Calculate Accelerometer Pitch and Roll 
  
  Pitch_R = atan(acc.x() / acc.z());
  //Roll_R = atan(acc.y() / acc.z());
  Pitch_A = Pitch_R * 180 / 3.145;
  //Roll_A = Roll_R * 180 / 3.145;
  Pitch_A_GOOD = !isnan(Pitch_A);

  //Calculate Gyro Pitch and Roll
  TimeStamp = millis();
  dt = (TimeStamp - TimeStamp_Pre)/1000.;
  Pitch_Angle_Change = -gyro.y() * dt;
  PitchG_A = PitchG_A + Pitch_Angle_Change;
  TimeStamp_Pre = TimeStamp;
  PitchG_A_GOOD = !isnan(Pitch_A);

  // Calculate Acc Filter values
  if (Pitch_A_GOOD && PitchG_A_GOOD ) {
    PitchF_A = (PitchF_A + Pitch_Angle_Change)* (Percent_Use_G/100.) + (Pitch_A * (100.-Percent_Use_G)/100.);
  }
  else {
    PitchF_A =0;
  }
  

  //Output check
  CountDown = TimeStamp - Toggle_Time;
  if (CountDown > 1000) {
    Red_State = !Red_State;
    White_State = !White_State;
    Blue_State = !Blue_State;
    Toggle_Time = TimeStamp;
  }
digitalWrite(Red_Pin, Red_State);
//digitalWrite(White_Pin, White_State);
//digitalWrite(Blue_Pin, Blue_State);
  //Serial.print(acc.x());
  //Serial.print(",");
  //Serial.print(acc.y());
  //Serial.print(",");
  //Serial.print(acc.z());
  //Serial.print(",");
  //Serial.print(gyro.x());
  //Serial.print(",");
  //Serial.print(gyro.y());
  //Serial.print(",");
  //Serial.print(gyro.z());
  //Serial.print(",");
  //Serial.print(Pitch_A);
  //Serial.print(",");  //Serial.print(dt);
  //Serial.print(",");
  //Serial.print(PitchG_A);
  //Serial.print(",");
  //Serial.print(PitchF_A);
  //Serial.print(",");
  //Serial.print(Percent_Use_G);
  //Serial.print(",");
  //Serial.print(accel);
  //Serial.print(",");
  //Serial.print(gyro);
  //Serial.print(",");
  //Serial.print(mg);
  //Serial.print(",");
  //Serial.print(sys);
  //Serial.print(",");
  Serial.print(Red_State);
  Serial.print(",");
  Serial.print(CountDown);
  Serial.println();

  delay (BNO055_SAMPLERATE_DELAY_MS);

}
