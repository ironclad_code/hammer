#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BNO055.h>
#include <utility/imumaths.h>
#include <math.h>

#define BNO055_SAMPLERATE_DELAY_MS (50)

Adafruit_BNO055 myIMU = Adafruit_BNO055();

//Accelorometer Variables
double Pitch_R = 0; //Pitch from Accelorometer in Radians
double Roll_R = 0; //Roll from Acc in Radians
double Pitch_A = 0; //Pitch from Accelorometer in degrees
double Roll_A = 0; //Roll from Accelorometer in degrees
double Roll_F = 0; //Roll from Accelorometer Filtered in Degrees
double Pitch_F = 0; //Pitch from Accelorometer Filtered in Degrees
double Roll_F_Pre = 0; //Previous Roll from Accelorometer Filtered in Degrees
double Pitch_F_Pre = 0; //Previous Pitch from Accelorometer Filtered in Degrees
double PercentFilter = 90; //Percent to prioritize the previous values over the current value

//Gyro Variables


void setup() {
  // put your setup code here, to run once:
//start Serial
Serial.begin(115200);

//Start IMU
myIMU.begin();
delay(1000);
myIMU.setExtCrystalUse(true);

}

void loop() {

//Read Calibration Levels
  //uint8_t sys_C, gyro_C, accel_C, mg_C =0;
  //myIMU.getCalibration(&sys_C, &gyro_C, &accel_C, &mg_C);


// Acquire Vectors from IMU
  imu::Vector<3> acc = myIMU.getVector(Adafruit_BNO055::VECTOR_ACCELEROMETER);
  imu::Vector<3> gyro = myIMU.getVector(Adafruit_BNO055::VECTOR_GYROSCOPE);
  //imu::Vector<3> mag = myIMU.getVector(Adafruit_BNO055::VECTOR_ACCELEROMETER);
  
 // Calculate Accelerometer Pitch and Roll 
  
  Pitch_R = atan(acc.x() / acc.z());
  Roll_R = atan(acc.y() / acc.z());
      Pitch_A = Pitch_R * 180 / 3.145;
    Roll_A = Roll_R * 180 / 3.145;
  
  // Calculate Acc Filter values
  if (isnan(Pitch_A))  {
    Pitch_F = 0;
  }
  else {
    PercentFilter = 98;
    if (abs(gyro.y()) > 2) {
      PercentFilter = 100 - PercentFilter;
    }
    Pitch_F = ((PercentFilter/100) * Pitch_F_Pre) + ((100 - PercentFilter)/100 * Pitch_A);
    Pitch_F_Pre = Pitch_F;
   //Roll_F = PercentFilter/100 * Roll_F_Pre + (100-PercentFilter)/100 * Roll_A;
   //Roll_F_Pre = Roll_F;

  }
  Serial.print(Pitch_A,3);
  Serial.print(",");
  //Serial.print(Roll_A,3);
  //Serial.print(",");
  Serial.print(Pitch_F,3);
  Serial.print(",");
  //Serial.print(Roll_F,3);
  //Serial.print(",");
  //Serial.print(gyro.x());
  Serial.print(",");
  Serial.print(gyro.y());
  //Serial.print(",");
  //Serial.print(gyro.z());
  //Serial.print(",");
  //Serial.print(accel);
  //Serial.print(",");
  //Serial.print(gyro);
  //Serial.print(",");
  //Serial.print(mg);
  //Serial.print(",");
  //Serial.print(sys);
  Serial.println();



  //Serial.print("gyro - ");
  //Serial.print(gyro.x());
  //Serial.print(",");
  //Serial.print(gyro.y());
  //Serial.print(",");
  //Serial.println(gyro.z());

  //Serial.print("mag - ");
  //Serial.print(mag.x());
  //Serial.print(",");
  //Serial.print(mag.y());
  //Serial.print(",");
  //Serial.println(mag.z());

  delay (BNO055_SAMPLERATE_DELAY_MS);

}
