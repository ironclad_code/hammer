//----------VARIABLE SETUP--------------

#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BNO055.h>
#include <utility/imumaths.h>
#include <math.h>
#include <FastLED.h>

//Button LED Defines
// How many leds in your strip?
#define NUM_LEDS 1
//Define DATA_PIN
#define BUTTON_MULTY_LED_PIN 6
#define BUTTON_INPUT_PIN 2
// Define the array of leds
CRGB leds[NUM_LEDS];

//Define Output LED pins
#define HAMMER_RED_LED_PIN 9
#define HAMMER_WHITE_LED_PIN 11
#define HAMMER_BLUE_LED_PIN 10

//BNO055 Defines
//Define sample rate
#define BNO055_SAMPLERATE_DELAY_MS (10)
//Create muIMU object
Adafruit_BNO055 myIMU = Adafruit_BNO055();

//Accelorometer Variables
double Pitch_R = 0; //Pitch from Gyro in Radians
double Roll_R = 0; //Roll from Gyro in Radians
double Pitch_A = 0; //Pitch from Gyro in degrees
double Roll_A = 0; //Roll from Gyro in degrees
double Roll_F = 0; //Roll from Gyro Filtered in Degrees
double Pitch_F = 0; //Pitch from Gyro Filtered in Degrees
double Roll_F_Pre = 0; //Previous Roll from Gyro Filtered in Degrees
double Pitch_F_Pre = 0; //Previous Pitch from Gyro Filtered in Degrees
double PercentFilter = 90; //Percent to prioritize the previous values over the current value


//Output Hammer LED Variables
int PWM_LED_Red = 255;
int PWM_LED_White = 255;
int PWM_LED_Blue = 255;

const int H_RED[] = {255,0,0};
const int H_WHITE[] = {0,255,0};
const int H_BLUE[] = {0,0,255};
const int H_GREY[] = {0,0,100};
const int H_BLACK[] = {0,0,0};
const int H_ALL[] = {255,255,255};
int H_Color[3];
int H_Flash[3];


//define States
int Vertical_State = 0; // 0-Hammer head down or Horizontal 1-Hammer head tilted roughly 45 deg 2-Hammer tilted Up semi vertical
int TempVertical_State =0;
int PrevVertical_State = 0; //Previous Vertical State
unsigned long Vertical_State_ChangeTime = 0;
int Swing_State =0;//hammer swing  state (0-Low motion 1-Medium motion 2-High motion)
float hswing = 0; //hammer swing gyro value
int Output_State =0; // Output state for hammer LEDs.  0-Off 1-solid on 2-flashiing 3-flashing 4-All ON

//Push Button Interupt Variable
volatile byte Action_State = 0; // 0-Red 1-Blue 2-White 3-Complete

//Other Variables
unsigned int Elapsed_Time = 0;
unsigned long COLOR_TimeStamp = millis();
unsigned long Current_Time = millis();
unsigned long Previous_Time = 0;
long COLOR = 0;

//------------Program SETUP----------------

void setup() {
   //Setup Serial USB port to use 115200 baud rate
  Serial.begin(115200);

  //Start IMU
  myIMU.begin();
  delay(1000); // wait for IMU to setup
  myIMU.setExtCrystalUse(true); //use IMU timing crystal instead of Arduinos for IMU timing

  /*Setup Iluminaed LED on push button Switch 
  Part on Amazon  https://www.amazon.com/ChromaTek-Full-Color-Momentary-Arduino-Raspberry/dp/B0988YTLNL?pd_rd_w=WNsx5&content-id=amzn1.sym.80b2efcb-1985-4e3a-b8e5-050c8b58b7cf&pf_rd_p=80b2efcb-1985-4e3a-b8e5-050c8b58b7cf&pf_rd_r=2WXD0MG7ZJP2NRZ3SWWF&pd_rd_wg=bBvK6&pd_rd_r=dcb3ed32-1ab8-44d8-9113-81bd12a4195b&pd_rd_i=B0988YTLNL&psc=1&ref_=pd_bap_d_grid_rp_0_1_ec_pd_nav_hcs_rp_4_t
  */
  FastLED.addLeds<WS2812, BUTTON_MULTY_LED_PIN, GRB>(leds, NUM_LEDS);  // GRB ordering is typical
  // Setup ouput pins
  pinMode(HAMMER_RED_LED_PIN, OUTPUT);
  pinMode(HAMMER_WHITE_LED_PIN, OUTPUT);
  pinMode(HAMMER_BLUE_LED_PIN, OUTPUT);
  // Setup input pins
  pinMode(BUTTON_INPUT_PIN, INPUT_PULLUP);
  //Setup push Button interupt to call "ChangeState" function on a press of the button (Falling edge) 
  attachInterrupt(digitalPinToInterrupt(BUTTON_INPUT_PIN),ChangeState,FALLING);
}

//---------------MAIN PROGRAM----------------

void loop() {
  //basic timining functions
  Previous_Time = Current_Time;
  Current_Time = millis();
  Elapsed_Time = Current_Time - Previous_Time;
  
  //Read Calibration Levels
  //uint8_t sys_C, gyro_C, accel_C, mg_C =0;
  //myIMU.getCalibration(&sys_C, &gyro_C, &accel_C, &mg_C);
  
  // Acquire Vectors from IMU
  imu::Vector<3> grav = myIMU.getVector(Adafruit_BNO055::VECTOR_GRAVITY); // Vector that shows acceleration for gravity and filters out other accelerations 3 axes
  imu::Vector<3> gyro = myIMU.getVector(Adafruit_BNO055::VECTOR_GYROSCOPE); // Vector for Gyroscope that report rotational velocity in Radiains/sec for 3 axes
  //imu::Vector<3> mag = myIMU.getVector(Adafruit_BNO055::VECTOR_ACCELEROMETER); // Vector for Accelerometer all accelerations 3 axes

  //Determing Vertical State from grav reading
  PrevVertical_State = TempVertical_State;
  if (grav.x() > 2){
    TempVertical_State = 2;//Hammer head semi vertical
  }
  else if (grav.x() > 2){
    TempVertical_State = 1;//Hammer head tilted
  }
  else {
    TempVertical_State = 0;//Hammer head not tilited or tilted down
  }
  if (TempVertical_State != PrevVertical_State){ //start timer on TempVertical_State change
    Vertical_State_ChangeTime = Current_Time;
  }
  if (Current_Time - Vertical_State_ChangeTime > 100) { //TempVertical State must be stable for 500ms to change Vertical_State
        Vertical_State = TempVertical_State;
    }
   

//determine hswing
hswing = (abs(gyro.z())); //zaxis only (head of hammer moving toward ground)
if (hswing > 150){
    Swing_State = 2;// high motion 
  }
  else if (hswing > 50){
    Swing_State = 1;// med motion
  }
  else {
    Swing_State = 0;// low motion
  }

  //Set Button LED color and Hammer LED colors based on Action State
  switch (Action_State){
  case 0: //Red
    COLOR = CRGB::Red;
    H_SetColor (H_Color, H_RED);
    H_SetColor (H_Flash, H_WHITE);
  break;
  case 1: //Blue
    COLOR = CRGB::Blue;
    H_SetColor (H_Color, H_BLUE);
    H_SetColor (H_Flash, H_WHITE);
  break;
  case 2: //White
    COLOR = CRGB::White;
    H_SetColor (H_Color, H_WHITE);
    H_SetColor (H_Flash, H_BLACK);
  break;
  default:
    COLOR = CRGB::Yellow; //Complete
    H_SetColor (H_Color, H_BLACK);
    H_SetColor (H_Flash, H_BLACK);
  }

  // Set Output State from Vertical state
  switch (Vertical_State) {
    case 0: //Horizontal or tilted down
      Output_State = 0; //Off
    break;
    case 1: //tilted up
      Output_State = 1; // Solid ON
    break;
    case 2: //semi vertical
      if (Swing_State == 2){ //high motion
        Output_State = 2; //Flashing
      }
      else {
        Output_State = 1; //Solid On
      }
    break;
  }

  switch (Output_State){
    case 0: //Off
      HammerWriteA(H_BLACK);
    break;
    case 1: //Solid On
      HammerWriteA(H_Color);
    break;
    case 2: //Flashing
      HammerFlash ();
    break;
    case 3: //Flashing
      HammerFlash ();
    break;
    default: //ALL ON
      HammerWriteA(H_ALL);
  }
  
    //SET button LED color output
   leds[0] = COLOR;
  FastLED.show();
 
  
  //Debug prints
  //Serial.print(hswing,1);
  //Serial.print(",");
  //Serial.print(grav.y(),1);
  //Serial.print(",");
  //Serial.print(grav.z(),1);
  //erial.print(",");
  //Serial.print(gyro.x(),1);
  //Serial.print(",");
  //Serial.print(gyro.y(),1);
  //Serial.print(",");
  //Serial.print(gyro.z());
  //Serial.print(",");
  //Serial.print(Vertical_State);
  //Serial.print(",");
  //Serial.print(Swing_State);
  //Serial.print(",");
  //Serial.print(Output_State);
  //Serial.print(",");
  //Serial.print(mg);
  //Serial.print(",");
  //Serial.print(sys);
  //Serial.println();

  delay (BNO055_SAMPLERATE_DELAY_MS);

}

//----------FUNCTIONS-----------------
//hammerFlashing sequence
void HammerFlash (){
  HammerWriteA(H_Flash);
  delay(300);
  HammerWriteA(H_Color);
  delay (300);
  for (int i = 0; i <= 6; i++){
    HammerWriteA(H_Flash);
    delay(50);
    HammerWriteA(H_Color);
    delay (150);
  }
  for (int i = 0; i <= 15; i++){
    HammerWriteA(H_Flash);
    delay(25);
    HammerWriteA(H_Color);
    delay (75);
  }
  HammerWriteA(H_Color);
  delay (2000);
  Action_State = 3;//Cpmplete
}

// Write Hammer LED output function
void HammerWriteA (int color[3]) {
//analogWrite values from 0 to 255
  analogWrite(HAMMER_RED_LED_PIN, color[0]);
  analogWrite(HAMMER_WHITE_LED_PIN, color[1]);
  analogWrite(HAMMER_BLUE_LED_PIN, color[2]);
}

//Copy Color from one to another
void H_SetColor (int color[3], int setcolor[3]) {
  for (int i = 0; i < 3; i++){
    color[i] = setcolor[i];
  }
}

//Button Interupt Function
void ChangeState () {
  static unsigned long last_interrupt_time = 0; //this step is done only on the first call due to the static statement??
  unsigned long interrupt_time = millis();
  // If interrupts come faster than 200ms, assume it's a bounce and ignore
  if (interrupt_time - last_interrupt_time > 200)   {
   Action_State = Action_State + 1; //There are only 3 action states (0-Red 1-White 2-Blue)
    if(Action_State > 2) {
      Action_State = 0;
    }
  }
  last_interrupt_time = interrupt_time;
}
